﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliveryCo.Business.Components.Interfaces;
using System.Transactions;
using DeliveryCo.Business.Entities;
using System.Threading;
using DeliveryCo.Services.Interfaces;


namespace DeliveryCo.Business.Components
{
    public class DeliveryProvider : IDeliveryProvider
    {
        public Guid SubmitDelivery(DeliveryCo.Business.Entities.DeliveryInfo pDeliveryInfo)
        {
            IDeliveryNotificationService lService = DeliveryNotificationServiceFactory.GetDeliveryNotificationService(pDeliveryInfo.DeliveryNotificationAddress);
            using (TransactionScope lScope = new TransactionScope())
            using (DeliveryCoEntityModelContainer lContainer = new DeliveryCoEntityModelContainer())
            {
                pDeliveryInfo.DeliveryIdentifier = Guid.NewGuid();
                pDeliveryInfo.Status = (int)DeliveryInfoStatus.Submitted;
                lContainer.DeliveryInfo.Add(pDeliveryInfo);
                lContainer.SaveChanges();
                ThreadPool.QueueUserWorkItem(new WaitCallback((pObj) => ScheduleDelivery(pDeliveryInfo)));
                lScope.Complete();
            }
            return pDeliveryInfo.DeliveryIdentifier;
        }

        private void ScheduleDelivery(DeliveryInfo pDeliveryInfo)
        {
            
            Console.WriteLine("Delivering to" + pDeliveryInfo.DestinationAddress);
            Thread.Sleep(1000);
            IDeliveryNotificationService lService = DeliveryNotificationServiceFactory.GetDeliveryNotificationService(pDeliveryInfo.DeliveryNotificationAddress);
            lService.NotifyDeliveryStatus(pDeliveryInfo.DeliveryIdentifier, Guid.Parse(pDeliveryInfo.OrderNumber), DeliveryInfoStatus.Submitted);
            Console.WriteLine("Setting delivery ID for bookstore");
            lService.SetDeliveryId(pDeliveryInfo.DeliveryIdentifier, Guid.Parse(pDeliveryInfo.OrderNumber));

            Console.WriteLine("Goods picked up");
            Thread.Sleep(1000);
            using (TransactionScope lScope = new TransactionScope())
            using (DeliveryCoEntityModelContainer lContainer = new DeliveryCoEntityModelContainer())
            {
                pDeliveryInfo.Status = (int)DeliveryInfoStatus.PickedUp;
                lService.NotifyDeliveryStatus(pDeliveryInfo.DeliveryIdentifier, Guid.Parse(pDeliveryInfo.OrderNumber), DeliveryInfoStatus.PickedUp);
            }
            Console.WriteLine("Out for delivery");
            Thread.Sleep(1000);
            using (TransactionScope lScope = new TransactionScope())
            using (DeliveryCoEntityModelContainer lContainer = new DeliveryCoEntityModelContainer())
            {
                pDeliveryInfo.Status = (int)DeliveryInfoStatus.OutForDelivery;
                lService.NotifyDeliveryStatus(pDeliveryInfo.DeliveryIdentifier, Guid.Parse(pDeliveryInfo.OrderNumber), DeliveryInfoStatus.OutForDelivery);
            }
            //notifying of delivery completion
            using (TransactionScope lScope = new TransactionScope())
            using (DeliveryCoEntityModelContainer lContainer = new DeliveryCoEntityModelContainer())
            {
                pDeliveryInfo.Status = (int)DeliveryInfoStatus.Delivered;
                lService.NotifyDeliveryStatus(pDeliveryInfo.DeliveryIdentifier, Guid.Parse(pDeliveryInfo.OrderNumber), DeliveryInfoStatus.Delivered);
            }

        }
    }
}
    