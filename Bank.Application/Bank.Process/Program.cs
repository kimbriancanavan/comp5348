﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Bank.Business.Entities;
using System.ServiceModel;
using Bank.Services;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using System.Messaging;

namespace Bank.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            ResolveDependencies();
            CreateDummyEntities();
            HostServices();
        }
        private static void QueueServices()
        {
            MessageQueue bookStoreQueue = EnsureQueueExistAndReturn(".\\private$\\bookStoreQueue");
            MessageQueue bankQueue = EnsureQueueExistAndReturn(".\\private$\\bankQueue");

            //test message sending
            ListeningToQueue(bankQueue);
            EnableSendOneMessage(bookStoreQueue);

        }
        private static void EnableSendOneMessage(MessageQueue mq)
        {
            Console.WriteLine("Type the message you want to send");
            String s = Console.ReadLine();
            Message msg = new Message();
            msg.Formatter = new BinaryMessageFormatter();
            msg.Body = s;

            if (mq.Transactional)
            {

                MessageQueueTransaction transaction = new MessageQueueTransaction();
                transaction.Begin();
                mq.Send(msg, transaction);
                transaction.Commit();
            }
            else
            {
                mq.Send(msg);
            }
            Console.WriteLine("message sent");

        }
        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void ListeningToQueue(MessageQueue mq)
        {
            mq.PeekCompleted += new PeekCompletedEventHandler(MyPeekCompleted);
            mq.BeginPeek();
            Console.WriteLine("Start Listening to queue");
        }

        private static void MyPeekCompleted(Object source, PeekCompletedEventArgs asyncResult)
        {
            //Connect to the queue
            MessageQueue mq = (MessageQueue)source;

            MessageQueueTransaction transaction = new MessageQueueTransaction();
            try
            {
                transaction.Begin();
                Message msg = mq.Receive(transaction);
                msg.Formatter = new BinaryMessageFormatter();
                ProcessMessage(msg);
                transaction.Commit();

            }
            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode == MessageQueueErrorCode.TransactionUsage)
                {
                    Console.WriteLine("Queue is not transactional");

                    transaction.Abort();
                }
            }
            //--d
            mq.EndPeek(asyncResult.AsyncResult);
            Console.WriteLine("A asynchronous peek operation is completed");

            mq.BeginPeek();
            return;
        }
        //process the message according to the message label and body
        private static void ProcessMessage(Message pMessage)
        {
            //do nothing yet
            String s = (String)pMessage.Body;
            Console.WriteLine(s);
        }

        private static void HostServices()
        {
            using (ServiceHost lHost = new ServiceHost(typeof(TransferService)))
            {
                lHost.Open();
                Console.WriteLine("Bank Services started. Press Q to quit.");
                QueueServices();
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }
        }

        private static void CreateDummyEntities()
        {
            using (TransactionScope lScope = new TransactionScope())
            using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            {
                if (lContainer.Accounts.Count() == 0)
                {
                    Customer lBookStore = new Customer();
                    Account lBSAccount = new Account() { AccountNumber = 123, Balance = 0 };
                    lBookStore.Accounts.Add(lBSAccount);

                    Customer lCustomer = new Customer();
                    Account lCustAccount = new Account() { AccountNumber = 456, Balance = 200 };
                    lCustomer.Accounts.Add(lCustAccount);

                    lContainer.Customers.Add(lBookStore);
                    lContainer.Customers.Add(lCustomer);

                    lContainer.SaveChanges();
                    lScope.Complete();
                }
            }

            // testing Transfer code
            //using (TransactionScope lScope = new TransactionScope())
            //using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            //{
            //    try
            //    {
            //        int pFromAcctNumber = 456;
            //        int pToAcctNumber = 123;
            //        int pAmount = 50;

            //        // find the two account entities and add them to the Container
            //        Account lFromAcct = lContainer.Accounts.Where(account => pFromAcctNumber == account.AccountNumber).First();
            //        Account lToAcct = lContainer.Accounts.Where(account => pToAcctNumber == account.AccountNumber).First();

            //        // update the two accounts
            //        lFromAcct.Withdraw(pAmount);
            //        lToAcct.Deposit(pAmount);

            //        // save changed entities and finish the transaction
            //        lContainer.SaveChanges();
            //        lScope.Complete();
            //    }
            //    catch (Exception lException)
            //    {
            //        Console.WriteLine("Error occured while transferring money:  " + lException.Message);
            //        throw;
            //    }
            //}
        }

        private static void ResolveDependencies()
        {
            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
    }
}
