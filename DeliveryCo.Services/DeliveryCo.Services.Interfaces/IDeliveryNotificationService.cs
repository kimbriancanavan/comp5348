﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace DeliveryCo.Services.Interfaces
{
    public enum DeliveryInfoStatus { Submitted, Delivered, Failed, PickedUp, OutForDelivery }

    [ServiceContract]
    public interface IDeliveryNotificationService
    {
        [OperationContract]
        void NotifyDeliveryStatus(Guid pDeliveryId, Guid pOrderNumber, DeliveryInfoStatus status);


        [OperationContract]
        void SetDeliveryId(Guid pDeliveryId, Guid pOrderNumber);
    }
}
