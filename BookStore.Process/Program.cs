﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using BookStore.Services;
using System.ServiceModel.Configuration;
using System.Configuration;
using System.ComponentModel.Composition.Hosting;
using BookStore.Services.Interfaces;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using BookStore.Business.Entities;
using System.Transactions;
using System.ServiceModel.Description;
using BookStore.Business.Components.Interfaces;
using BookStore.WebClient.CustomAuth;
using System.Messaging;

namespace BookStore.Process
{
    public class Program
    {
        static void Main(string[] args)
        {
            ResolveDependencies();
            InsertDummyEntities();
            HostServices();
        }
        private static void QueueServices()
        {
            MessageQueue bookStoreQueue = EnsureQueueExistAndReturn(".\\private$\\bookStoreQueue");
            MessageQueue bankQueue = EnsureQueueExistAndReturn(".\\private$\\bankQueue");

            //test message sending
            ListeningToQueue(bookStoreQueue);
            EnableSendOneMessage(bankQueue);

        }
        private static void EnableSendOneMessage(MessageQueue mq)
        {
            Console.WriteLine("Type the message you want to send");
            String s = Console.ReadLine();
            Message msg = new Message();
            msg.Formatter = new BinaryMessageFormatter();
            msg.Body = s;

            if (mq.Transactional)
            {

                MessageQueueTransaction transaction = new MessageQueueTransaction();
                transaction.Begin();
                mq.Send(msg, transaction);
                transaction.Commit();
            }
            else
            {
                mq.Send(msg);
            }
            Console.WriteLine("message sent");

        }
        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void ListeningToQueue(MessageQueue mq)
        {
            
            mq.PeekCompleted += new PeekCompletedEventHandler(MyPeekCompleted);
            mq.BeginPeek();
            Console.WriteLine("Start listening to Queue");
        }

        private static void MyPeekCompleted(Object source, PeekCompletedEventArgs asyncResult)
        {
            //Connect to the queue
            MessageQueue mq = (MessageQueue)source;

            MessageQueueTransaction transaction = new MessageQueueTransaction();
            try
            {
                transaction.Begin();
                Message msg = mq.Receive(transaction);
                msg.Formatter = new BinaryMessageFormatter();
                ProcessMessage(msg);          
                transaction.Commit();

            }
            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode == MessageQueueErrorCode.TransactionUsage)
                {
                    Console.WriteLine("Queue is not transactional");

                    transaction.Abort();
                }
            }
            //--
            mq.EndPeek(asyncResult.AsyncResult);
            Console.WriteLine("A asynchronous peek operation is completed");

            mq.BeginPeek();
            return;
        }
        //process the message according to the message label and body
        private static void ProcessMessage(Message pMessage)
        {
            //do nothing yet
            String s = (String)pMessage.Body;
            Console.WriteLine(s);
        }

        private static void InsertDummyEntities()
        {
            InsertCatalogueEntities();
            CreateOperator();
            CreateUser();
        }

        private static void CreateUser()
        {
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                if (lContainer.Users.Where((pUser) => pUser.Name == "Customer").Count() > 0)
                    return;
            }

           
            User lCustomer = new User()
            {
                Name = "Customer",
                LoginCredential = new LoginCredential() { UserName = "Customer", Password = "COMP5348" },
                Email = "David@Sydney.edu.au",
                Address = "1 Central Park",
                BankAccountNumber = 456,
            };

            ServiceLocator.Current.GetInstance<IUserProvider>().CreateUser(lCustomer);
        }

        private static void InsertCatalogueEntities()
        {
            using (TransactionScope lScope = new TransactionScope())
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                if (lContainer.Books.Count() == 0)
                {

                    var houses = new List<Warehouse>();

                    for (int i = 0; i < 11; i++)
                    {
                        Warehouse lHouse = new Warehouse()
                        {
                            Name = String.Format("Warehouse {0}", i.ToString())
                        };

                        lContainer.Warehouses.Add(lHouse);
                        houses.Add(lHouse);
                    }

                    Book lGreatExpectations = new Book()
                    {
                        Author = "Jane Austen",
                        Genre = "Fiction",
                        Price = 20.0,
                        Title = "Pride and Prejudice"
                    };

                    lContainer.Books.Add(lGreatExpectations);

                    Stock lGreatExpectationsStock = new Stock()
                    {
                        Book = lGreatExpectations,
                        Quantity = 5,
                        Warehouse = houses[2]
                    };

                    lContainer.Stocks.Add(lGreatExpectationsStock);

                    Book lSoloist = new Book()
                    {
                        Author = "Charles Dickens",
                        Genre = "Fiction",
                        Price = 15.0,
                        Title = "Grape Expectations"
                    };

                    lContainer.Books.Add(lSoloist);

                    Stock lSoloistStock = new Stock()
                    {
                        Book = lSoloist,
                        Quantity = 7,
                        Warehouse = houses[3]
                    };

                    lContainer.Stocks.Add(lSoloistStock);

                    for (int i = 1; i < 10; i++)
                    {
                        Book lItem = new Book()
                        {
                            Author = String.Format("Author {0}", i.ToString()),
                            Genre = String.Format("Genre {0}", i),
                            Price = i,
                            Title = String.Format("Title {0}", i)
                        };

                        lContainer.Stocks.Add(lSoloistStock);

                        for (int j = 0; j < 11; j++)
                        {
                            Stock lStock = new Stock()
                            {
                                Book = lItem,
                                Quantity = 10 + i + j,
                                Warehouse = houses[j]
                            };

                            lContainer.Stocks.Add(lStock);
                        }


                    }

                    lContainer.SaveChanges();
                    lScope.Complete();
                }
            }
        }

   

        private static void CreateOperator()
        {
            Role lOperatorRole = new Role() { Name = "Operator" };
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                if (lContainer.Roles.Count() > 0)
                {
                    return;
                }
            }
            User lOperator = new User()
            {
                Name = "Operator",
                LoginCredential = new LoginCredential() { UserName = "Operator", Password = "COMP5348" },
                Email = "Wang@Sydney.edu.au",
                Address = "1 Central Park"
            };

            lOperator.Roles.Add(lOperatorRole);

            ServiceLocator.Current.GetInstance<IUserProvider>().CreateUser(lOperator);
        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }


        private static void HostServices()
        {
            List<ServiceHost> lHosts = new List<ServiceHost>();
            try
            {

                Configuration lAppConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ServiceModelSectionGroup lServiceModel = ServiceModelSectionGroup.GetSectionGroup(lAppConfig);

                System.ServiceModel.Configuration.ServicesSection lServices = lServiceModel.Services;
                foreach (ServiceElement lServiceElement in lServices.Services)
                {
                    ServiceHost lHost = new ServiceHost(Type.GetType(GetAssemblyQualifiedServiceName(lServiceElement.Name)));
                    lHost.Open();
                    lHosts.Add(lHost);
                }
                Console.WriteLine("BookStore Service Started, press Q key to quit");
                QueueServices();
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }
            finally
            {
                foreach (ServiceHost lHost in lHosts)
                {
                    lHost.Close();
                }
            }
        }

        private static String GetAssemblyQualifiedServiceName(String pServiceName)
        {
            return String.Format("{0}, {1}", pServiceName, System.Configuration.ConfigurationManager.AppSettings["ServiceAssemblyName"].ToString());
        }
    }
}
