﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using System.Messaging;
using EmailService.Business.Components.Interfaces;
using EmailService.Services;


namespace EmailService.Process
{
    class Program
    {

        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        static void Main(string[] args)
        {
            ResolveDependencies();
            using (ServiceHost lHost = new ServiceHost(typeof(EmailService.Services.EmailService)))
            {
                lHost.Open();
                Console.WriteLine("Email Service Started");
                QueueServices();
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }
        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
        private static void QueueServices()
        {
            MessageQueue emailQueue = EnsureQueueExistAndReturn(".\\private$\\emailQueue");

            //test message sending
            ListeningToQueue(emailQueue);
          //  EnableSendOneMessage(emailQueue);

        }
        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void ListeningToQueue(MessageQueue mq)
        {
            mq.PeekCompleted += new PeekCompletedEventHandler(MyPeekCompleted);
            mq.BeginPeek();
            Console.WriteLine("Start Listening to queue");
        }

        private static void MyPeekCompleted(Object source, PeekCompletedEventArgs asyncResult)
        {
            //Connect to the queue
            MessageQueue mq = (MessageQueue)source;
            mq.Formatter = new XmlMessageFormatter(new Type[]{typeof(EmailService.Business.Entities.EmailMessage)});
            MessageQueueTransaction transaction = new MessageQueueTransaction();

            try
            {
                transaction.Begin();
                Message msg = mq.Receive(transaction);         
                ProcessMessage(msg);
                transaction.Commit();
            }
            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode == MessageQueueErrorCode.TransactionUsage)
                {
                    Console.WriteLine("Queue is not transactional");

                    transaction.Abort();
                }
            }
            //--d
            mq.EndPeek(asyncResult.AsyncResult);
            Console.WriteLine("A asynchronous peek operation is completed");

            mq.BeginPeek();
            return;
        }
        //process the message according to the message label and body
        private static void ProcessMessage(Message pMessage)
        {
            EmailService.Services.EmailService es = new EmailService.Services.EmailService();
            EmailService.Business.Entities.EmailMessage email = (EmailService.Business.Entities.EmailMessage)pMessage.Body;
            es.EmailProvider.SendEmail(email);
        }
    }
}
