﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookStore.Business.Components.Interfaces;
using BookStore.Business.Entities;
using Microsoft.Practices.ServiceLocation;
using System.Transactions;

namespace BookStore.Business.Components
{
    public class DeliveryNotificationProvider : IDeliveryNotificationProvider
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public void SetDeliveryId(Guid pDeliveryId, Guid pOrderNumber)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                Order lAffectedOrder = RetrieveOrder(pOrderNumber);
                if (lAffectedOrder != null)
                {
                    lAffectedOrder.Delivery.ExternalDeliveryIdentifier = pDeliveryId;
                    lContainer.SaveChanges();
                }
                lScope.Complete();
            }
        }

        public void NotifyDeliveryStatus(Guid pDeliveryId, Guid pOrderNumber, Entities.DeliveryStatus status)
        {
            Order lAffectedOrder = RetrieveOrder(pOrderNumber);
            UpdateDeliveryStatus(pOrderNumber, status);
            String lBookStoreMessage, lCustomerMessage;

            switch (status)
            {
                case DeliveryStatus.Delivered:
                    lCustomerMessage = "Our records show that your order" + lAffectedOrder.OrderNumber + " has been delivered. Thank you for shopping at video store";
                    lBookStoreMessage = "Delivered order: " + lAffectedOrder.OrderNumber;
                    break;
                case DeliveryStatus.Failed:
                    lCustomerMessage = "Our records show that there was a problem" + lAffectedOrder.OrderNumber + " delivering your order. Please contact Book Store";
                    lBookStoreMessage = "Order failed: " + lAffectedOrder.OrderNumber;
                    break;
                case DeliveryStatus.PickedUp:
                    lCustomerMessage = "Our records show that your order " + lAffectedOrder.OrderNumber + " has been picked up";
                    lBookStoreMessage = "Order picked up: " + lAffectedOrder.OrderNumber;
                    break;
                case DeliveryStatus.OutForDelivery:
                    lCustomerMessage = "Our records show that your order " + lAffectedOrder.OrderNumber + " is out for delivery";
                    lBookStoreMessage = "Order out for delivery: " + lAffectedOrder.OrderNumber;
                    break;
                case DeliveryStatus.Submitted:
                    lCustomerMessage = null;
                    lBookStoreMessage = "Order submitted: " + lAffectedOrder.OrderNumber;
                    break;
                default:
                    throw new Exception("Unexpected DeliveryStatus");
            }
            if (lCustomerMessage != null)
            {

                EmailProvider.SendMessage(new EmailMessage()
                {
                    ToAddress = lAffectedOrder.Customer.Email,
                    Message = lCustomerMessage
                });
            }
            if (lBookStoreMessage != null)
            {
                EmailProvider.SendMessage(new EmailMessage()
                {
                    ToAddress = "bookstore",
                    Message = lBookStoreMessage
                });
            }
        

        }

        private void UpdateDeliveryStatus(Guid pOrderNumber, DeliveryStatus status)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                Delivery lDelivery = lContainer.Deliveries.Where((pDel) => pDel.Order.OrderNumber == pOrderNumber).FirstOrDefault();
                if (lDelivery != null)
                {
                    lDelivery.DeliveryStatus = status;
                    lContainer.SaveChanges();
                }
                lScope.Complete();
            }
        }

        private Order RetrieveDeliveryOrder(Guid pDeliveryId)
        {
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                Delivery lDelivery = lContainer.Deliveries.Include("Order.Customer").Where((pDel) => pDel.ExternalDeliveryIdentifier == pDeliveryId).FirstOrDefault();
                return lDelivery.Order;
            }
        }

        private Order RetrieveOrder(Guid pOrderNumber)
        {
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                Order lOrder = lContainer.Orders.Include("Delivery").Include("Customer").Where((pOrder) => pOrder.OrderNumber == pOrderNumber).FirstOrDefault();
                return lOrder;
            }
        }
    }


}