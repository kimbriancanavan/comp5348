﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookStore.Business.Components.Interfaces;
using System.Messaging;

namespace BookStore.Business.Components
{
    public class EmailProvider : IEmailProvider
    {
        public void SendMessage(EmailMessage pMessage)
        {
            // ENQUEUE EMAIL

            MessageQueue emailQueue = EnsureQueueExistAndReturn(".\\private$\\emailQueue");
            global::EmailService.MessageTypes.EmailMessage msg = new global::EmailService.MessageTypes.EmailMessage()
            {
                Message = pMessage.Message,
                ToAddresses = pMessage.ToAddress,
                Date = DateTime.Now
            };

            // DO SOMETHING :)

            SendOneMessage(emailQueue, msg);

            //ExternalServiceFactory.Instance.EmailService.SendEmail
            //(
            //new global::EmailService.MessageTypes.EmailMessage()
            //{
            //Message = pMessage.Message,
            //ToAddresses = pMessage.ToAddress,
            //Date = DateTime.Now
            //}
            //);
        }
        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void SendOneMessage(MessageQueue mq, global::EmailService.MessageTypes.EmailMessage pEmailMessage)
        {
            if (mq.Transactional)
            {
                MessageQueueTransaction transaction = new MessageQueueTransaction();
                transaction.Begin();
                mq.Send(pEmailMessage, transaction);
                transaction.Commit();
            }
            else
            {
                mq.Send(pEmailMessage);
            }
            Console.WriteLine("message sent");

        }
    }
}
