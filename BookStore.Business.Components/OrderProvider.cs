﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookStore.Business.Components.Interfaces;
using BookStore.Business.Entities;
using System.Transactions;
using Microsoft.Practices.ServiceLocation;
using DeliveryCo.MessageTypes;
using BookStore.Business.Components.Interfaces;
using System.Messaging;


namespace BookStore.Business.Components
{
    public class OrderProvider : IOrderProvider
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public IUserProvider UserProvider
        {
            get { return ServiceLocator.Current.GetInstance<IUserProvider>(); }
        }

        public void SubmitOrder(Entities.Order pOrder)
        {      
            using (TransactionScope lScope = new TransactionScope())
            {
                //LoadBookStocks(pOrder);
                //MarkAppropriateUnchangedAssociations(pOrder);

                using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
                {
                    try
                    {
                        pOrder.OrderNumber = Guid.NewGuid();
                        pOrder.Store = "OnLine";

                        var houses = lContainer.Warehouses.ToList();

                        // Book objects in pOrder are missing the link to their Stock tuple (and the Stock GUID field)
                        // so fix up the 'books' in the order with well-formed 'books' with 1:1 links to Stock tuples
                        foreach (OrderItem lOrderItem in pOrder.OrderItems)
                        {
                            int bookId = lOrderItem.Book.Id;
                            lOrderItem.Book = lContainer.Books.Where(book => bookId == book.Id).First();
                            //The above line seems to add the reference to stocks so im not sure why the bottm lines were added for the link.


                            // List<Stock> stocks = lOrderItem.Book.Stocks.ToList();
                            // List<Stock> ting = lContainer.Stocks.Where(stock => stocks.Contains(stock.Id)).ToList();

                            //System.Guid stockId = lOrderItem.Book.Stock.Id;
                            // lOrderItem.Book.Stocks = lContainer.Stocks.Where(stock => stockId == stock.Id).First();
                        }

                        List<Warehouse> warehouses = lContainer.Warehouses.ToList();
                        Warehouse deliveryWarehouse = null;

                        foreach (Warehouse warehouse in warehouses)
                        {
                            bool warehouseFound = true;

                            foreach (OrderItem lOrderItem in pOrder.OrderItems)
                            {
                                int? stockCount = 0;
                                Stock currentstock = warehouse.Stocks.Where(stock => lOrderItem.Book.Id == stock.Book.Id).FirstOrDefault();
                                if (currentstock != null) stockCount = currentstock.Quantity;

                                if (lOrderItem.Quantity > stockCount)
                                {
                                    Console.WriteLine("Not enough stock for " + lOrderItem.Book.Title + " in " + warehouse.Name);
                                    warehouseFound = false;
                                    break;
                                }

                            }

                            if (warehouseFound) { deliveryWarehouse = warehouse; break; }
                        }

                        if (deliveryWarehouse != null)
                        {
                            Console.WriteLine("Warehouse found for delivery " + deliveryWarehouse.Name);
                            pOrder.Warehouse = deliveryWarehouse.Name;
                        }
                        else
                        { throw new Exception("Warehouse not available to fulfill order"); }
                        // and update the stock levels
                        pOrder.UpdateStockLevels(deliveryWarehouse);

                        // add the modified Order tree to the Container (in Changed state)
                        lContainer.Orders.Add(pOrder);
                        
                        //--
                        //send a transfer request (id,  and amount )
                        //wait for a respond
                        // receive respond: -> process to the nex step
                        // new queueManager,
                        // queueMan
                        // ask the Bank service to transfer fundss
                        TransferFundsFromCustomer(UserProvider.ReadUserById(pOrder.Customer.Id).BankAccountNumber, pOrder.Total ?? 0.0);
                        //--


                        // ask the delivery service to organise delivery
                        PlaceDeliveryForOrder(pOrder);

                        // and save the order
                        lContainer.SaveChanges();
                        lScope.Complete();                    
                    }
                    catch (Exception lException)
                    {
                        SendOrderErrorMessage(pOrder, lException);
                        IEnumerable<System.Data.Entity.Infrastructure.DbEntityEntry> entries =  lContainer.ChangeTracker.Entries();
                        throw;
                    }
                }
            }
            SendOrderPlacedConfirmation(pOrder);
        }

        //private void MarkAppropriateUnchangedAssociations(Order pOrder)
        //{
        //    pOrder.Customer.MarkAsUnchanged();
        //    pOrder.Customer.LoginCredential.MarkAsUnchanged();
        //    foreach (OrderItem lOrder in pOrder.OrderItems)
        //    {
        //        lOrder.Book.Stock.MarkAsUnchanged();
        //        lOrder.Book.MarkAsUnchanged();
        //    }
        //}

        private void LoadBookStocks(Order pOrder)
        {
            using (BookStoreEntityModelContainer lContainer = new BookStoreEntityModelContainer())
            {
                foreach (OrderItem lOrderItem in pOrder.OrderItems)
                {
                   // lOrderItem.Book.Stock = lContainer.Stocks.Where((pStock) => pStock.Book.Id == lOrderItem.Book.Id).FirstOrDefault();    
                }
            }
        }

        private void SendOrderErrorMessage(Order pOrder, Exception pException)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "There was an error in processsing your order " + pOrder.OrderNumber + ": "+ pException.Message + ". Please contact Book Store"
            });
        }

        private void SendOrderPlacedConfirmation(Order pOrder)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "Your order " + pOrder.OrderNumber + " has been placed"
            });
        }

        private void PlaceDeliveryForOrder(Order pOrder)
        {
            Delivery lDelivery = new Delivery() { DeliveryStatus = DeliveryStatus.Submitted, SourceAddress = "Book Store Address", DestinationAddress = pOrder.Customer.Address, Order = pOrder };
            pOrder.Delivery = lDelivery;


            MessageQueue deliveryQueue = EnsureQueueExistAndReturn(".\\private$\\deliveryCoQueue");
            DeliveryInfo msg = new DeliveryInfo()
            {
                OrderNumber = lDelivery.Order.OrderNumber.ToString(),
                SourceAddress = lDelivery.SourceAddress,
                DestinationAddress = lDelivery.DestinationAddress,
                DeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService"
            };

            // DO SOMETHING :)

            SendOneMessage(deliveryQueue, msg);

            //ExternalServiceFactory.Instance.DeliveryService.SubmitDelivery(new DeliveryInfo()
            //{
            //    OrderNumber = lDelivery.Order.OrderNumber.ToString(),
            //    SourceAddress = lDelivery.SourceAddress,
            //    DestinationAddress = lDelivery.DestinationAddress,
            //    DeliveryNotificationAddress = "net.tcp://localhost:9010/DeliveryNotificationService"
            //});

            //lDelivery.ExternalDeliveryIdentifier = lDeliveryIdentifier;
        }

        private void TransferFundsFromCustomer(int pCustomerAccountNumber, double pTotal)
        {
            try
            {
                ExternalServiceFactory.Instance.TransferService.Transfer(pTotal, pCustomerAccountNumber, RetrieveBookStoreAccountNumber());
            }
            catch
            {
                throw new Exception("Error when transferring funds for order.");
            }
        }


        private int RetrieveBookStoreAccountNumber()
        {
            return 123;
        }

        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void SendOneMessage(MessageQueue mq, DeliveryInfo pDeliveryMessage)
        {
            if (mq.Transactional)
            {
                MessageQueueTransaction transaction = new MessageQueueTransaction();
                transaction.Begin();
                mq.Send(pDeliveryMessage, transaction);
                transaction.Commit();
            }
            else
            {
                mq.Send(pDeliveryMessage);
            }
            Console.WriteLine("message sent");

        }


    }
}
