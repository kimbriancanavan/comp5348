﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DeliveryCo.Services;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using System.Messaging;
using EmailService;
using DeliveryCo.MessageTypes;

namespace DeliveryCo.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            ResolveDependencies();
            using (ServiceHost lHost = new ServiceHost(typeof(DeliveryService)))
            {
                lHost.Open();
                Console.WriteLine("Delivery Service started. Press Q to quit");
                QueueServices();
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }

        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
        private static void QueueServices()
        {
            MessageQueue emailQueue = EnsureQueueExistAndReturn(".\\private$\\emailQueue");
            MessageQueue deliveryCoQueue = EnsureQueueExistAndReturn(".\\private$\\deliveryCoQueue");

            //test message sending
            ListeningToQueue(deliveryCoQueue);
            //SendEmail(emailQueue);
                

        }

        private static void SendEmail(MessageQueue mq)
        {
            Message msg = new Message();
            msg.Formatter = new BinaryMessageFormatter();
            EmailService.Business.Entities.EmailMessage e = new EmailService.Business.Entities.EmailMessage();
            e.Message = "your order is picked up";
            e.ToAddresses = "sending to address 1";

            msg.Body = e;
            msg.Label = "Pickedup";

            if (mq.Transactional)
            {

                MessageQueueTransaction transaction = new MessageQueueTransaction();
                transaction.Begin();
                mq.Send(e, transaction);
                transaction.Commit();
            }
            else
            {
                mq.Send(msg);
            }
            Console.WriteLine("picked up message sent");
        }
        private static MessageQueue EnsureQueueExistAndReturn(String queuePath)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                return MessageQueue.Create(queuePath, true);
            }
            else
            {
                MessageQueue mq = new MessageQueue(queuePath);
                return mq;
            }

        }
        private static void ListeningToQueue(MessageQueue mq)
        {
            mq.PeekCompleted += new PeekCompletedEventHandler(MyPeekCompleted);
            mq.BeginPeek();
            Console.WriteLine("Start Listening to queue");
        }

        private static void MyPeekCompleted(Object source, PeekCompletedEventArgs asyncResult)
        {
            //Connect to the queue
            MessageQueue mq = (MessageQueue)source;
            mq.Formatter = new XmlMessageFormatter(new Type[] { typeof(DeliveryInfo) });

            MessageQueueTransaction transaction = new MessageQueueTransaction();
            try
            {
                transaction.Begin();
                Message msg = mq.Receive(transaction);
                //msg.Formatter = new BinaryMessageFormatter();
                ProcessMessage(msg);
                transaction.Commit();

            }
            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode == MessageQueueErrorCode.TransactionUsage)
                {
                    Console.WriteLine("Queue is not transactional");

                    transaction.Abort();
                }
            }
            //--d
            mq.EndPeek(asyncResult.AsyncResult);
            mq.BeginPeek();
            return;
        }
        //process the message according to the message label and body
        private static void ProcessMessage(Message pMessage)
        {
            try
            {
                //do nothing yet
                DeliveryInfo pDeliveryInfo = (DeliveryInfo)pMessage.Body;
                DeliveryCo.Services.DeliveryService ds = new DeliveryCo.Services.DeliveryService();
                ds.SubmitDelivery(pDeliveryInfo);
            }
            catch(Exception message)
            {
                Console.WriteLine(message);
            }

        }
    }
}
